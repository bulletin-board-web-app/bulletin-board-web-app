/*
* NOTE: You can define all __PARAMS__ manually 
*       previously get them from config file - main.cfg
*
* $Id: ddscript.sql,v 1.7 2011-12-14 18:06:48 ruslan Exp $ 
*/

/* begin tables creation */

create database __DATABASE__;

grant all privileges on __DATABASE__.* to '__DBUSER__'@'__HOST__';
set password for '__DBUSER__'@'__HOST__' = password('__PASSWORD__');

use __DATABASE__;

create table users
 (
    user_id smallint unsigned not null auto_increment,
    username varchar(20) not null,
    email varchar(40) not null,
    homepage varchar(20),
    constraint pk_userid primary key (user_id)
 );

create table messages
 (
    msg_id smallint unsigned not null auto_increment,
    message text not null,
    user_id smallint unsigned not null,
	pub_date datetime not null,
    ipaddress varchar(15) not null,
    user_agent varchar(100) not null,
    constraint pk_msg_user primary key (msg_id, user_id),
	constraint fk_user_id foreign key (user_id) 
        references users (user_id)
 );

/* end table creation */

