/*
 * validator.js - JS script to validate web form
 *
 * 
 * $Id: validator.js,v 1.7 2011-12-13 13:27:07 ruslan Exp $
 */

var formFields = {
    "username": {
        "mandatory": true,
        "min": 5,
        "max": 10,
        "regexp": "\\W+",
        "note": "use alphanumeric symbols",
        "strip_html": false,
    },
    "email": {
        "mandatory": true,
        "regexp": false,
        "strip_html": false,
    },
    "message": {
        "mandatory": true,
        "regexp": false,
        "strip_html": true,
    },
    "captcha": {
        "mandatory": true,
        "regexp": false,
        "strip_html": false,
    }
}

function validateForm(form) {
    var errCounter = 0;
    var errFields = {};

    for( var field in formFields ) {
        if ( formFields[field]['mandatory'] ) {
            var result = checkIsEmpty(form, field);
            if (result) {
                errFields[field] = result;
                errCounter++;
            }
        }
        if ( formFields[field]['regexp'] ) {
            var result = checkRegexp(form, field);
            if (result) {
                errFields[field] = result;
                errCounter++;
            }
        }
    }

    if (errCounter > 0) {
        printError(errFields);
        return false;
    }
    else {
        return true;
    }
}

function checkIsEmpty(form, element) {
    if ( form.elements[element].value == "" ) {
        return(element + " could not be empty!");
    }
    return false;
}


function checkRegexp(form, element) {
    if ( form.elements[element].value ) {
        var val = form.elements[element].value;
        var re = new RegExp(formFields[element]['regexp']);
        if ( re.test(val) ) {
            return(element + " field contains illegal chars (" + formFields[element]['note'] + ").");
        }
        else {
            return false;
        }
    }
}

function printError(msgArray) {
    var errMsg = "<p>WARNING:";
    for (var key in msgArray) {
        errMsg = errMsg + "<br>" + msgArray[key];
    }
    errMsg = errMsg + "</p>";
    document.getElementById("js_error").innerHTML = errMsg;
}
