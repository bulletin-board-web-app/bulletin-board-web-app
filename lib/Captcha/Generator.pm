#
#===============================================================================
#
#         FILE:  Generator.pm
#
#  DESCRIPTION:
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Ruslan A.Afanasiev (), <ruslan.afanasiev@gmail.com>
#      COMPANY:
#      VERSION:  1.0
#      CREATED:  09.12.2011 13:18:38 EET
#     REVISION:  $Id: Generator.pm,v 1.11 2011-12-14 19:29:30 ruslan Exp $
#===============================================================================
package Captcha::Generator;

=head1 NAME

Captcha::Generator - a module to generate 'captcha' images.

=head1 SYNOPSIS


    use CGI;
    use Captcha::Generator;
    my $cpo = Captcha::Generator->new();
    my $cgi = CGI->new();

    # get captcha from the CGI form
    my $captcha = $cgi->param('captcha');

    # check the captcha
    if ( $cpo->check_captcha($captcha) ) {
        # do something
    }


=head1 DESCRIPTION

This module can do following things: 
    - generate a random captcha image as a SVG document
    - convert the SVG document into a GIF file
    - verify user's input captcha
    - etc

=cut

use strict;
use warnings;
use fields qw( tmpdir imgdir ipaddress timeout cap_len );

BEGIN {
    use FindBin qw($RealBin);
    $RealBin =~ s/^(.+)\/\w+$/$1/;
    push(@INC, $RealBin);
}

use Tools::Config;

=head1 VERSION

Version 1.0

=cut

our $VERSION = "1.0";

## create list of symbols to generate the captcha
my @ALPHANUMS = ();
push( @ALPHANUMS, ( 'a' .. 'z' ), ( 'A' .. 'Z' ), ( 0 .. 9 ) );
my $CONVERT = '/usr/bin/convert';

=head2 Methods

=over 12

=item C<new>

Returns a Captcha::Generator object

=back

=cut

sub new {
    my $class = shift;
    my $self  = fields::new($class);
    ## do initializing
    $self->_init(@_);
    return $self;
}

=over 12

=item C<_init>

Initialize object's fields.

=back

=cut

sub _init {
    my $self = shift;
    if (@_) {
        my %fields = @_;
        @{$self}{ keys %fields } = values %fields;
    }
    else {
        $self->{'ipaddress'} = undef;
    }
    ## get config
    my $cfg = Tools::Config->new(); 
    $self->{'tmpdir'}  = $cfg->get_param('tmpdir');
    $self->{'imgdir'}  = $cfg->get_param('imgdir');
    $self->{'timeout'} = $cfg->get_param('timeout');
    $self->{'cap_len'} = $cfg->get_param('cap_len');

    ## check if there is a directory to keep images
    my $imgdir = $self->{'tmpdir'} . '/' . $self->{'imgdir'};
    unless ( -d $imgdir ) {
        mkdir($imgdir) or die "Can't create temp dir: $!";
    }
    ## otherwise clear old imgs in the directory
    else {
        $self->clear_old_images;
    }
}

=over 12

=item C<get_image>

get_image - generates SVG document, convert it into GIF or returns SVG document.

Arguments: none

=back

=cut

sub get_image {
    my $self = shift;
    my $rand_seq;

    ## get a random sequence of the symbols  
    for ( 1 .. $self->{'cap_len'} ) {
        $rand_seq .= $ALPHANUMS[ rand(@ALPHANUMS) ];
    }

    my $svgdoc = $self->build_svg_doc($rand_seq);

    my $imgdir  = $self->{'tmpdir'} . '/' . $self->{'imgdir'};
    my $svgfile = $imgdir . '/' . "$self->{'ipaddress'}-$rand_seq.svg";

    open( TMPIMG, '>', $svgfile ) or die "Can't write to SVG-file: $!";
    print TMPIMG $svgdoc;
    close TMPIMG;

    if ( $self->can_convert2gif ) {
        return $self->convert_svg2gif( $svgfile, $rand_seq );
    }
    else {
        return $svgdoc;
    }
}

=over 12

=item C<check_captcha>

check_captcha - compares user's input with captcha-file.
                Returns 1 (right) or 0 (wrong).

Arguments: 1

    $cap_response - user's input

=back

=cut

sub check_captcha {
    my $self         = shift;
    my $cap_response = shift;
    ## a filename should be IPADDRESS-CAPTCHA
    my $imgfile =
        $self->{'tmpdir'} . '/'
      . $self->{'imgdir'} . '/'
      . "$self->{'ipaddress'}-$cap_response";

    ## if the file exists   - captcha is valid
    ## otherwise            - captcha is wrong
    if ( -e "$imgfile.gif" ) {
        foreach (qw(gif svg)) {
            unlink "$imgfile.$_";
        }
        return 1;
    }
    else {
        return 0;
    }
}

=over 12

=item C<build_svg_doc>

build_svg_doc - builds the SVG document using a random symbols sequence

Arguments: 1

    $rand_seq - a random sequence of the symbols

=back

=cut

sub build_svg_doc {
    my $self     = shift;
    my $rand_seq = shift;

    my ( $x, $y ) = ( &get_rand_num, &get_rand_num );
    my ( $x1, $y1 ) = ( $x + 20, $y + 5 );

    my $svgdoc = <<"SVG";
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>
<svg viewBox="0 0 100 35" version="1.1">
    <text x="20" y="20" transform="rotate(10 $x,$y)" style="font-size:20; stroke:#000000; fill:#000000;">
        $rand_seq
    </text>
    <line x1="20" y1="20" x2="80" y2="$y" style="stroke: #000000"/>
    <line x1="$y" y1="$x1" x2="$y1" y2="$y" style="stroke: #000000"/>
    <line x1="$x1" y1="$y1" x2="$x" y2="$y" style="stroke: #000000"/>
    <line x1="$y1" y1="$x1" x2="$y" y2="$x" style="stroke: #000000"/>
</svg>
SVG
    return $svgdoc;
}

=over 12

=item C<clear_old_images>

clear_old_images - removes all images if they are old 

=back

=cut

sub clear_old_images {
    my $self = shift;

    opendir( IMGDIR, "$self->{'tmpdir'}/$self->{'imgdir'}" )
      or die "Can't open img directory: $!";
    ## find all the files which are old then TIMEOUT 
    my @imgs = grep {
        ( time() - (stat("$self->{'tmpdir'}/$self->{'imgdir'}/$_"))[9] >
              $self->{'timeout'} )
    } readdir(IMGDIR);
    closedir IMGDIR;

    ## remove all the old img files
    unlink "$self->{'tmpdir'}/$self->{'imgdir'}/$_" foreach (@imgs);
}

=over 12

=item C<can_convert2gif>

can_convert2gif - looks for 'convert' utility (see man convert)

=back

=cut

sub can_convert2gif {
    my $self = shift;
    ## if 'convert' utility exists - we can simply convert SVG into GIF
    if ( -e $CONVERT && -x $CONVERT ) {
        return 1;
    }
    else {
        return 0;
    }
}

=over 12

=item C<convert_svg2gif>

convert_svg2gif - converts SVG doc into GIF using 'convert' utility.

Arguments: 2

    $svgfile - SVG document stored as a file
    $rand_seq - a random sequence of the symbols

=back

=cut

sub convert_svg2gif {
    my $self     = shift;
    my $svgfile  = shift;
    my $rand_seq = shift;

    my $gifimage;
    my $giffile =
        $self->{'tmpdir'} . '/'
      . $self->{'imgdir'} . '/'
      . "$self->{'ipaddress'}-$rand_seq.gif";

    ## convert SVG image file into GIF using utility 'convert'
    system( $CONVERT, $svgfile, $giffile ) == 0
      or die "Error during convert SVG to GIF: $!";

    open( GIF, '<', $giffile ) or die "Can't read from GIF-file: $!";
    binmode GIF;
    $gifimage .= $_ while <GIF>;
    close GIF;
    return $gifimage;
}

=over 12

=item C<get_rand_num>

get_rand_num - 'helper' function to get 'pseudo-random' numbers.

=back

=cut

sub get_rand_num {
    my @range = 10 .. 50;
    return rand(@range);
}

=head1 AUTHOR

Ruslan Afanasiev, C<< <ruslan.afanasiev at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<ruslan.afanasiev at gmail.com>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.
    
    perldoc Captcha::Generator


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2011 Ruslan Afanasiev

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.

=cut

1;

__END__
