#
#===============================================================================
#
#         FILE:  DBQuery.pm
#
#  DESCRIPTION:
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Ruslan A.Afanasiev (), <ruslan.afanasiev@gmail.com>
#      COMPANY:
#      VERSION:  2.0
#      CREATED:  11.12.2011 18:35:44 EET
#     REVISION:  $Id: DBQuery.pm,v 1.12 2011-12-14 19:22:41 ruslan Exp $
#===============================================================================
package Storage::DBQuery;

=head1 NAME

Storage::DBQuery - a module that utilizes some methods for 'data minig'

=head1 SYNOPSIS

    use Storage::DBQuery;
    my $dbq = Storage::DBQuery->new();

    # define a column and an order method to sort the data
    my $order = [ qw(username desc) ];
   
    # define a limit for the messages quantity 
    # in this case we define a slice of the messages from 0 to 10
    my $limit = [ 0, 10 ];

    # get messages sorted by 
    my $msg = $dbq->run_query(
        'select_all_messages', 
        { args => $limit, order => $order}, 
    );

=head1 DESCRIPTION

This module keeps a query map for SQL statements, provides a generic method to interact with DB
and  allows to sort the data by a rule.

=cut

use strict;
use warnings;
use fields qw(dbh);
use DBI;
use Data::Dumper;

BEGIN {
    use FindBin qw($RealBin);
    $RealBin =~ s/^(.+)\/\w+$/$1/;
    push(@INC, $RealBin);
}

use Tools::Config;

=head1 VERSION

Version 2.0

=cut

our $VERSION = "2.0";

=head1 DATABASE QUERY MAP (DBQMAP)
    
DBQMAP - a hash table to keep all the SQLs statements, methods what are needed to run
         these SQLs, subroutines (see the key 'dm_sub') which can sort the data.

         All the SQLs have a 'human readable' names:
         - insert_new_user 
         - insert_new_user
         - select_user_id_by_username
         - select_all_messages

=cut

my $DBQMAP = {
    insert_new_message => {
        sql         => 'INSERT INTO messages (message, user_id, pub_date, ipaddress, user_agent) 
                        VALUES (?, ?, ?, ?, ?)',
        methods     => [qw(execute)],
    },
    insert_new_user => {
        sql         => 'INSERT INTO users (username, email, homepage) VALUES (?, ?, ?)',
        methods     => [qw(execute)],
    },
    select_user_id_by_username => {
        sql         => 'SELECT user_id FROM users WHERE username = ?',
        methods     => [qw(execute fetchrow_array)],
    },
    select_all_messages => {
        sql         => 'SELECT u.username AS username, u.email AS email, m.pub_date AS pub_date, m.message AS message 
                        FROM messages m JOIN users u ON u.user_id = m.user_id ORDER BY pub_date DESC LIMIT ?, ?',
        methods     => [qw(execute fetchall_arrayref)],
        dm_sub      => \&do_sort_data,
    },
};

=head2 Methods/Subroutines

=over 12

=item C<new>

Returns a Storage::DBQuery object

=back

=cut

sub new {
    my $class = shift;
    my $self  = fields::new($class);
    $self->_init;
    return $self;
}

=over 12

=item C<_init>

Initialize object's fields.

=back

=cut

sub _init {
    my $self = shift;
    ## get config
    my $cfg = Tools::Config->new();
    ## create dbh
    my $dsource = 'DBI:mysql:database=' . $cfg->get_param('database') . ';host=' . $cfg->get_param('host');
    $self->{'dbh'} = DBI->connect( $dsource, $cfg->get_param('dbuser'), $cfg->get_param('password') )
      or die $DBI::errstr;
    ## check each method call if there is an error
    $self->{'dbh'}->{'RaiseError'} = 1;
}

=over 12

=item C<run_query>

run_query - finds SQL statement using DBQMAP, binds params
            and executes all the DBI methods what are needed for this type of SQL.
            It returns the total data set.

Arguments: 2
    
    $qtype - SQL statement type
    $param - list of params (array ref) to bind them or do something else whith them

=back

=cut

sub run_query {
    my $self    = shift;
    my $qtype   = shift;
    my $params  = shift;

    my $sth = $self->{'dbh'}->prepare( $DBQMAP->{$qtype}->{'sql'} );

    ## bind params 
    if ( exists $params->{'args'} ) {
        my $ph = 1;
        foreach (@{$params->{'args'}}) {
            $sth->bind_param( $ph, $_ );
            $ph++;
        }
    }

    ## execute all the methods what are defined for this type of SQL
    my $res; 
    foreach my $m (@{$DBQMAP->{$qtype}->{'methods'}}) {
        $res = $sth->$m(); 
    }

    ## run 'data mining' subroutine if it is needed
    (exists $DBQMAP->{$qtype}->{'dm_sub'}) 
        ? return $DBQMAP->{$qtype}->{'dm_sub'}->($res, $params->{'order'}) 
        : return $res;
}

=over 12

=item C<do_sort_data>

do_sort_data - a subroutine to sort the data by the order

Arguments: 2

    $data - inital data (array of arrays)
    $sort_order - array ref like this [qw(email desc)]

=back

=cut

sub do_sort_data {
    my $data       = shift;
    my $sort_order = shift;
    my $raw = [];

    my $sort_func = sub {
        if ( $sort_order->[1] eq 'asc' ) {
            uc($a->{$sort_order->[0]}) cmp uc($b->{$sort_order->[0]});
        }
        elsif ( $sort_order->[1] eq 'desc' ) {
            uc($b->{$sort_order->[0]}) cmp uc($a->{$sort_order->[0]});
        }
    };

    foreach my $r ( @{$data} ) {
        push(@$raw,{ 
            username => $r->[0], 
            email    => $r->[1], 
            pub_date => $r->[2], 
            message  => $r->[3] 
            });
    }
    return [ sort { $sort_func->() } @{$raw} ]; 
}

=head1 AUTHOR

Ruslan Afanasiev, C<< <ruslan.afanasiev at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<ruslan.afanasiev at gmail.com>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.
    
    perldoc Storage::DBQuery


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2011 Ruslan Afanasiev

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.

=cut

1;

