#
#===============================================================================
#
#         FILE:  Utils.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Ruslan A.Afanasiev (), <ruslan.afanasiev@gmail.com>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  14.12.2011 03:16:43 EET
#     REVISION:  $Id: Utils.pm,v 1.2 2011-12-14 14:30:12 ruslan Exp $
#===============================================================================
package Tools::Utils;

=head1 NAME

Tools::Utils - a module to store some usefull subroutines in one place

=head1 SYNOPSIS

    use Tools::Config qw(strip_html);

    my $htmlized_str = '<b>hello world</b>';
    # remove all HTML tags
    my $clear_str = strip_html($htmlized_str);


=head1 DESCRIPTION

This module keeps some subroutines in one 'namespace'.

=cut

use strict;
use warnings;
use vars qw( @ISA @EXPORT @EXPORT_OK );
use Exporter;

=head1 VERSION

Version 1.0

=cut

our $VERSION = "1.0";
@ISA = qw(Exporter);

@EXPORT = qw();
@EXPORT_OK = qw( get_time strip_html );

=head2 Functions

=over 12

=item C<get_time>

get_time - returns a current time as a string like '2011-12-12 10:10:55'

=back

=cut

sub get_time {
    my ( $sec, $min, $hour, $day, $month, $year ) = ( localtime(time) )[ 0, 1, 2, 3, 4, 5 ];
    $month++;
    $year += 1900;
    return sprintf("%d-%d-%d %02d:%02d:%02d", $year, $month, $day, $hour, $min, $sec);
}

=over 12

=item C<strip_html>

strip_html - removes all the HTML tags and returns a 'clean' string

=back

=cut

sub strip_html {
    my $str = shift;
    $str =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs if (defined $str);
    return $str;
}

=head1 AUTHOR

Ruslan Afanasiev, C<< <ruslan.afanasiev at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<ruslan.afanasiev at gmail.com>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.
    
    perldoc Tools::Utils


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2011 Ruslan Afanasiev

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.

=cut

1;
