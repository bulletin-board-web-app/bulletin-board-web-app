#
#===============================================================================
#
#         FILE:  Config.pm
#
#  DESCRIPTION:  
#
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Ruslan A.Afanasiev (), <ruslan.afanasiev@gmail.com>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  14.12.2011 12:47:55 EET
#     REVISION:  $Id: Config.pm,v 1.6 2011-12-14 17:56:54 ruslan Exp $
#===============================================================================
package Tools::Config;

=head1 NAME

Tools::Config - a module to get settings from the config file

=head1 SYNOPSIS

    use Tools::Config;
    my $cfg = Tools::Config->new();
    # e.g. get a dbuser param
    my $param = $cfg->get_param('dbuser');

=head1 DESCRIPTION

This module reads the config file, stores the configuration 
in the hash ref and return a setting by a key.

=cut

use strict;
use warnings;
use fields qw( cfg_file params );

use constant {
    CFG_FILE => '/var/www/conf/main.cfg',
};

=head1 VERSION

Version 1.0

=cut

our $VERSION = "1.0";

=head2 Methods

=over 12

=item C<new>

Returns a Tools::Config object

=back

=cut

sub new {
    my $class = shift;
    my $self  = fields::new($class);
    $self->_init;
    return $self;
}

=over 12

=item C<_init>

Initialize object's fields.

=back

=cut

sub _init {
    my $self = shift;
    $self->{'cfg_file'} = CFG_FILE;
    $self->{'params'}   = {};
    ## get the configuration
    $self->get_config;
}

=over 12

=item C<get_config>

get_config - reads the config file and puts the settings into the field 'param'.

=back

=cut

sub get_config {
    my $self = shift;

    open(CFG, '<', $self->{'cfg_file'}) or die "Can't open config file: $!";
    while ( my $line = <CFG> ) {
        next if ( $line =~ /^$/ or $line =~ /^#/);
        ## remove spaces
        $line =~ s/\s+//g;
        ## config file contains pare key=value
        my ($key, $val) = split(/=/, $line);
        $self->{'params'}->{$key} = $val;
    }
    close CFG;
}

=over 12

=item C<get_param>

get_param - returns a value by a key from 'params' field.

Arguments: 1
    
    $key - a key (string)

=back

=cut

sub get_param {
    my $self = shift;
    my $key = shift;
    return $self->{'params'}->{$key};
}

=head1 AUTHOR

Ruslan Afanasiev, C<< <ruslan.afanasiev at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<ruslan.afanasiev at gmail.com>.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.
    
    perldoc Tools::Config


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2011 Ruslan Afanasiev

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.

=cut
1;
