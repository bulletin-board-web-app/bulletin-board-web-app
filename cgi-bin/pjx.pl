#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  pjx.pl
#
#        USAGE:  ./pjx.pl
#
#  DESCRIPTION:
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Ruslan A.Afanasiev (), <ruslan.afanasiev@gmail.com>
#      COMPANY:
#      VERSION:  1.0
#      CREATED:  13.12.2011 17:24:50 EET
#     REVISION:  $Id: pjx.pl,v 1.6 2011-12-14 19:32:30 ruslan Exp $
#===============================================================================
use strict;
use warnings;
use lib qw(lib ../lib);
use CGI;
use HTML::Template;
use Storage::DBQuery;
use Data::Dumper;
use Tools::Config;

use constant {
    TMPL_DIR   => '../templates/',
    TABLE_TMPL => 'result_table.tmpl',
    LIMIT      => Tools::Config->new->get_param('limit'),
};

our $VERSION = "1.0";

my $q = CGI->new();
print $q->header();

my $table = HTML::Template->new(
    filename  => TMPL_DIR . TABLE_TMPL,
    associate => $q,
);

my $order_by = [ split( /-/, $q->url_param('selector') ) ];
my $num_page = $q->url_param('num_page');

my $messages = Storage::DBQuery->new()->run_query( 
    'select_all_messages',
    { args => [ $num_page, LIMIT ], order => $order_by } 
);

$table->param( RECORDS => $messages );
print $table->output;

#print Dumper($q);
#print 'Got args - ' . $q->url_param('num_page') .' '. $q->url_param('selector');
