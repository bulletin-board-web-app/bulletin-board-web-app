#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  capgen.pl
#
#        USAGE:  ./capgen.pl 
#
#  DESCRIPTION:  
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Ruslan A.Afanasiev (), <ruslan.afanasiev@gmail.com>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  09.12.2011 14:38:30 EET
#     REVISION:  $Id: capgen.pl,v 1.4 2011-12-14 12:40:32 ruslan Exp $
#===============================================================================

use strict;
use warnings;
use lib qw(lib ../lib);
use Captcha::Generator;

our $VERSION = "1.0";

my $capgen = Captcha::Generator->new(
    ipaddress => $ENV{'REMOTE_ADDR'},
);
print "Pragma: no-cache\n";              

if ($capgen->can_convert2gif) {
    print "Content-type: image/gif\n\n";
}
else {
    print "Content-type: image/svg+xml\n\n";
}
print $capgen->get_image;
