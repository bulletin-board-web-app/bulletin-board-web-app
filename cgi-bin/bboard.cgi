#!/usr/bin/perl
#===============================================================================
#
#         FILE:  bboard.cgi
#
#        USAGE:  ./bboard.cgi
#
#  DESCRIPTION:
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Ruslan A.Afanasiev (), <ruslan.afanasiev@gmail.com>
#      COMPANY:
#      VERSION:  3.0
#      CREATED:  08.12.2011 22:57:06 EET
#     REVISION:  $Id: bboard.cgi,v 1.32 2011-12-14 19:48:21 ruslan Exp $
#===============================================================================
use warnings;
use strict;
use lib qw(lib ../lib);
use CGI;
use CGI::Ajax;
use CGI::Carp 'fatalsToBrowser';
use HTML::Template;
use Data::Dumper;
use Captcha::Generator;
use Storage::DBQuery;
use Tools::Config;
use Tools::Utils qw( strip_html get_time );

our $VERSION = "3.0";
use constant {
    TMPL_DIR  => '../templates/',
    TEMPLATES => {
        header => 'header.tmpl',
        footer => 'footer.tmpl',
        body   => 'body.tmpl',
    },
    LIMIT        => Tools::Config->new->get_param('limit'),
    PJX_URL      => 'pjx.pl',
    DEFAULT_SORT => [qw(pub_date desc)],
    DEBUG        => 0,
};

#---------------------------------------------------------------------------
#  MAIN part of the script
#---------------------------------------------------------------------------

my $q   = CGI->new();
my $pjx = CGI::Ajax->new( func => PJX_URL );
$pjx->skip_header(1);
$pjx->JSDEBUG(1) if DEBUG;

my $HTML_OUT = '';

print $q->header();

my $tfooter = HTML::Template->new(
    filename  => TMPL_DIR . TEMPLATES->{'footer'},
    associate => $q,
);
$tfooter->param( VERSION => $VERSION );

my $theader = HTML::Template->new(
    filename  => TMPL_DIR . TEMPLATES->{'header'},
    associate => $q,
);
$theader->param( TITLE => 'Bulletin Board' );

$HTML_OUT .= $theader->output;

my $tbody;
## Parameters are defined, therefore the form has been submitted
if ( $q->param() ) {
    $tbody = get_tbody($q);
}
## Otherwise, compose the default template
else {
    $tbody = HTML::Template->new(
        filename  => TMPL_DIR . TEMPLATES->{'body'},
        associate => $q,
    );
}

## put messages into the body template
my $messages = get_all_messages( $q );
$tbody->param( RECORDS => $messages );

## show/hide the button 'Next page' 
if ( scalar(@{$messages}) >= LIMIT ) {
    $tbody->param( SHOW_NEXT_BUTTON => 1 );
}
else {
    $tbody->param( SHOW_NEXT_BUTTON => 0 );
}

## show/hide the button 'Prev page'
if ( $q->param('next_num') ) {
    $tbody->param( N_NUMBER => $q->param('next_num') );
    if ( grep { $_ eq 'prev_num'} $q->param ) {
        $tbody->param( P_NUMBER => $q->param('prev_num') );
        $tbody->param( SHOW_PREV_BUTTON => 1 ) if ($q->param('prev_num') >= 0);
    }
}

## define param NUM_PAGE because it is needed for the sorting routine
($q->param('page')) 
    ? $tbody->param( NUM_PAGE => $q->param('page') ) 
    : $tbody->param( NUM_PAGE => 0 );

# Output body and result table
$HTML_OUT .= $tbody->output;

# Output footer and end html
$HTML_OUT .= $tfooter->output;

# Print composed HTML page using the CGI::Ajax call 
print $pjx->build_html( $q, sub { return $HTML_OUT } );
#print Dumper($q);

exit(0);

#---------------------------------------------------------------------------
#   Subroutine creates the template whith the results of the form
#---------------------------------------------------------------------------
sub get_tbody {
    my $q = shift;

    ## get params from the input form
    my $username = $q->param('username');
    my $email    = $q->param('email');
    my $homepage = $q->param('homepage') || 'NULL';
    my $message  = strip_html( $q->param('message') );
    my $captcha  = $q->param('captcha');
    my $mdate    = &get_time;

    my $tbody = HTML::Template->new(
        filename  => TMPL_DIR . TEMPLATES->{'body'},
        associate => $q,
    );

    my $cpo = Captcha::Generator->new( ipaddress => $ENV{'REMOTE_ADDR'} );

    ## check the captcha, then insert/reject a new message
    if ( defined $captcha && $cpo->check_captcha( $captcha ) ) {
        insert_message( $username, $message, $email, $mdate, $homepage );
    }
    elsif ( defined $captcha && ! $cpo->check_captcha( $captcha ) ) {
        $tbody->param( 
            CAPTCHA_RES => "Wrong CAPTCHA response: $captcha", 
            SHOW_INPUT  => 1 
        );
    }
    return $tbody;
}

#---------------------------------------------------------------------------
#   Subroutine inserts the data into DB
#---------------------------------------------------------------------------
sub insert_message {
    my $username = shift;
    my $message  = shift;
    my $email    = shift;
    my $mdate    = shift;
    my $homepage = shift;

    my $dbctr = Storage::DBQuery->new();
    ## check if the user already exists
    my $uid = $dbctr->run_query( 'select_user_id_by_username', { args => [$username] } );

    if ( $uid ) {
        ## relate the message with the user
        my $retmsg =
          $dbctr->run_query( 
            'insert_new_message',
            { args => [ $message, $uid, $mdate, $ENV{'REMOTE_ADDR'}, $ENV{'HTTP_USER_AGENT'} ] }
        );
    }
    else {
        ## create a new user and relate this message with the new user
        my $retuser = $dbctr->run_query( 
            'insert_new_user',
            { args => [ $username, $email, $homepage ] }
        );
        $uid = $dbctr->run_query( 'select_user_id_by_username', { args => [$username] } );
        my $retmsg = $dbctr->run_query( 
            'insert_new_message',
            { args => [ $message, $uid, $mdate, $ENV{'REMOTE_ADDR'}, $ENV{'HTTP_USER_AGENT'} ] }
        );
    }
}

#---------------------------------------------------------------------------
#   Subroutine gets messages from DB using the limit
#---------------------------------------------------------------------------
sub get_all_messages {
    my $q        = shift;
    my $limits   = [];
    my $order_by = [];

    ## get params to retrieve the data sorted by order 
    ( $q->param('selector')) 
        ? push( @$order_by, split(/-/, $q->param('selector')) ) 
        : push( @$order_by, @{ +DEFAULT_SORT });

    ## get a number for a next page to define a limit
    if ( $q->param('page') && $q->param('page') =~ /\d+/ ) {
        push(@$limits, $q->param('page'), LIMIT);
        $q->param( -name => 'next_num', -value => $limits->[0] + LIMIT );
        $q->param( -name => 'prev_num', -value => $limits->[0] - LIMIT );
    }
    else {
        push(@$limits, 0, LIMIT);
        $q->param( -name => 'next_num', -value => LIMIT );
    }

    ##print '<br>LIMITS: ' . Dumper($limits) . '<br>';
    ## get the messages limited by $limits and sorted by $order_by
    ## $limits = [ START_INDEX, MSG_QUANTITY ]
    ## $order_by = [ COLUMN, (desc|asc) ]
    return Storage::DBQuery->new()->run_query(
        'select_all_messages', { args => $limits, order => $order_by }
    );
}

__END__
