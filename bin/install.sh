#!/bin/bash
#===============================================================================
#
#          FILE:  install.sh
# 
#         USAGE:  ./install.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Ruslan A. Afanasiev (), ruslan.afanasiev@gmail.com
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  14.12.2011 16:32:30 EET
#      REVISION:  $Id: install.sh,v 1.7 2011-12-14 19:12:33 ruslan Exp $
#===============================================================================

# array of the directories what are needed for the web-app
APP_DIRS=(cgi-bin lib conf css js templates)

# array of the mandatory Perl modules for the web-app
PERL_MODULES=(CGI::Ajax HTML::Template)

WWW_HOME=/var/www           # where is located DOCUMENT_ROOT
DB_SCRIPT=../db/ddscript.sql   # SQL-script to create DB
CONFIG=../conf/main.cfg
WWW_USER=www-data
WWW_GROUP=www-data


#-------------------------------------------------------------------------------
#   Main part of the script - checking prerequisite, copying, etc
#-------------------------------------------------------------------------------
echo "============================================="
echo "Begin checking prerequisite..."
echo "============================================="

WHO=$(/usr/bin/whoami)
## check the user - the installation requires root's priviliges
echo -n "Checking user priviliges"
if [[ "$WHO" != "root" ]]; then
    echo " - not ok [ you need to be root to install web-app! ]"
    exit
fi
echo " - ok [ $WHO ]"

OSTYPE=$(/bin/uname)
## check the OS - script knows how install the web-app under Linux
echo -n "Checking OS platform    "
if [[ "$OSTYPE" != "Linux" ]]; then
    echo " - not ok [ Don't know how to install under $OSTYPE ]"
    echo "Please, read README file to do installation manually"
    exit
else
    echo " - ok [ $OSTYPE -" $(cat /etc/issue|awk '{print $1,$2}') "]"
fi

## check Perl modules
echo -n "Checking Perl modules   "
for m in ${PERL_MODULES[*]}; do
    RV=`perl -e "use $m; print 'ok'"`
    if [ -z "$RV" ]; then
        echo " [ Perl module $m not found. Use CPAN to install ]"
        exit
    fi
done
## Perl modules are installed
echo " - ok [ found mandatory modules ${PERL_MODULES[@]} ]"

echo -n "Perform the installation? (y|n): "
read ANSWER

case "$ANSWER" in
    "Y"|"y")
        echo "============================================="
        echo "Begin installation..."
        echo "============================================="

        for i in ${APP_DIRS[*]}; do
            if [ -d ${WWW_HOME}/${i} ]; then
                echo " - Dir ${WWW_HOME}/$i already exists"
            else
                echo " - Dir $i does not exist under $WWW_HOME"
                echo " - Creating..."
                mkdir ${WWW_HOME}/${i}
            fi
            echo " - Copying files to ${WWW_HOME}/${i}"
            cp -r ../${i}/* ${WWW_HOME}/${i}
            #echo $i
        done

        echo "Copying files done!"
        echo "============================================="
        echo "Chowning files to ${WWW_USER}:${WWW_GROUP}..."
        
        for i in ${APP_DIRS[*]}; do
            chown -R $WWW_USER:$WWW_GROUP ${WWW_HOME}/${i}
        done
        
        echo "Chowning files done!"
        echo "============================================="

        echo "Change mode to a+x for all cgi scripts..."
        echo "Found cgi scripts:" ${WWW_HOME}/cgi-bin/*.{cgi,pl}

        chmod a+x ${WWW_HOME}/cgi-bin/*
        
        echo "Done!"
        echo "============================================="

        echo "Preparing the SQL script..."
        ## read the params from the config
        echo "Read the config file to get settings for Database from $CONFIG"

        source $CONFIG
        
        echo "Got settings for Database:"
        echo " - DBUSER: $dbuser"
        echo " - DATABASE: $database"
        echo " - PASSWORD: $password"
        echo " - HOST: $host"
        ## change SQL script in-place
        echo "Substitute wildcards in SQL script by real params"

        perl -pi -e "s/__DBUSER__/$dbuser/g" $DB_SCRIPT
        perl -pi -e "s/__DATABASE__/$database/g" $DB_SCRIPT
        perl -pi -e "s/__PASSWORD__/$password/g" $DB_SCRIPT
        perl -pi -e "s/__HOST__/$host/g" $DB_SCRIPT
        
        echo "Substituting of SQL script done!"
        echo "============================================="

        ## load SQL script into MySQL
        echo "You need to input MySQL root password to load the SQL script."
        mysql -u root -p < $DB_SCRIPT

        if [ $? -eq 0 ]; then
            echo "Database created, schema loaded!"
        else
            echo "Wrong root password for MySQL!"
            exit
        fi

        echo "Exiting..."
        exit
    ;;
    "N"|"n")
        echo "Bye"
        exit
    ;;
    *)
        echo "Unknown case! Please input 'y' or 'n'."; exit
    ;;
esac

# END
